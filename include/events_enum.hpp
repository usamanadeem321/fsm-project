#ifndef EVENTS_ENUM_H
#define EVENTS_ENUM_H

enum class events_enum {
  event_lock = 1,
  event_unlock,
  event_open,
  event_close
};

#endif
