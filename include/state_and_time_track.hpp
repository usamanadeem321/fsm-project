#ifndef STATE_AND_TIME_TRACK
#define STATE_AND_TIME_TRACK

#include <ctime>
#include <string>

class state_and_time_track {
public:
  static std::string current_state;
  static int current_time;
  static bool time_bw_9and6;
  static void checktime() {
    time_t ttime = time(0);

    tm *local_time = localtime(&ttime);

    if (local_time->tm_hour >= 9 && local_time->tm_hour < 18) {
      time_bw_9and6 = true;
    }

    else {
      time_bw_9and6 = false;
    }
  }
};
std::string state_and_time_track::current_state;
int state_and_time_track::current_time = 9;
bool state_and_time_track::time_bw_9and6;

#endif