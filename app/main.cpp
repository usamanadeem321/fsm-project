#include "events_enum.hpp"
#include "msfsm.hpp"
#include "state_and_time_track.hpp"
#include <iostream>

using namespace msfsm;
using namespace std;

class my_fsm : protected Fsm<my_fsm> {
public:
  my_fsm() { transition(st_locked, Request(0)); }

  void moveTo(int event_id) {
    cout << "Moving to Event:  " << event_id << endl;
    handle(Request(event_id));
  }

private:
  friend Fsm;

  // Events
  struct Request {
    const int event_id;
    Request(int f) : event_id(f) {}
  };

  class State : public Fsm::State,
                public Named<State>,
                public state_and_time_track {

    friend Fsm;
    using Fsm::State::State;
    void entry(Request r) {
      cout << "Entering " << name() << endl;
      state_and_time_track ::current_state = string(name());

      event(r);
    }
    virtual void event(Request) = 0;
  };

  class state_Closed : public State {
    friend Fsm;
    using State::State;
    void event(Request r) override {
      if (r.event_id == int(events_enum::event_open)) {
        transition(fsm.st_opened, 0);
      } else if (r.event_id == int(events_enum::event_lock)) {
        transition(fsm.st_locked, 0);
      } else if (r.event_id == int(events_enum::event_close)) {
        cout << "Door Already Closed (and locked)" << endl;
      } else if (r.event_id == int(events_enum::event_unlock)) {
        cout << "Door Already Unlocked" << endl;
      }
    }

  } st_closed{this};

  class state_Opened : public State {
    friend Fsm;
    using State::State;
    void event(Request r) override {
      if (r.event_id == int(events_enum::event_lock)) {
        cout << "Opened Door cannot be locked, Close first!" << endl;
      } else if (r.event_id == int(events_enum::event_unlock)) {
        cout << "Already Unlocked" << endl;
      } else if (r.event_id == int(events_enum::event_open)) {
        cout << "Door already opened" << endl;
      } else if (r.event_id == int(events_enum::event_close)) {
        transition(fsm.st_closed, 0);
      }
    }
  } st_opened{this};

  class state_Locked : public State {
    friend Fsm;
    using State::State;
    void event(Request r) override {
      if (r.event_id == int(events_enum::event_unlock)) {

        state_and_time_track::checktime();
        if (state_and_time_track ::time_bw_9and6) {
          transition(fsm.st_closed, 0);
        } else {
          cout << "Time not between 9am and 6pm " << name() << endl;
        }

      } else if (r.event_id == int(events_enum::event_close) ||
                 r.event_id == int(events_enum::event_open) ||
                 r.event_id == int(events_enum::event_lock)) {
        cout << "Already Locked and Closed" << endl;
      }
    }
  } st_locked{this};
};

void options() {

  cout << "1- Lock \n";
  cout << "2- Unlock \n";
  cout << "3- Open \n";
  cout << "4- Close \n";
  cout << "5- Quit \n";
}

int main() {

  my_fsm my_machine;
  int event_no = -1;
  enum event_selection { Lock = 1, Unlock = 2, Open = 3, Close = 4, Quit = 5 };
  options();
  while (1) {
    cout << endl << state_and_time_track::current_state << endl;

    cout << "Select Event: ";
    while (!(cin >> event_no)) {
      std::cout << "Enter an Integer !!!" << std::endl;
      cin.clear();
      cin.ignore(10, '\n');
    }
    switch (event_no) {
    case Lock:
      my_machine.moveTo(int(events_enum::event_lock));
      break;
    case Unlock:
      my_machine.moveTo(int(events_enum::event_unlock));
      break;
    case Open:
      my_machine.moveTo(int(events_enum::event_open));
      break;
    case Close:
      my_machine.moveTo(int(events_enum::event_close));
      break;
    case Quit:
      exit(0);

    default:
      std::cout << "Invalid Event Selection" << std::endl;
      break;
    }
  }

  return 0;
}